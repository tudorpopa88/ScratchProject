import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

public class MyTest2 {

	@Test
	public void rockingTest() {
		System.setProperty("webdriver.gecko.driver", "C://SeleniumDrivers//geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		driver.get("http://www.amazon.com");
		String title = driver.getTitle();
		System.out.println("Page title: " + title);
	}
	
}
